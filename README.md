
---

### What Is It? ###

Scrapes mailto: contacts from web pages for a given cidr range or ip addresses

---

### How do I get set up? ###

make run

scrapper:load_cidr("../../cidrs.txt") OR scrapper:load_ips("../../ips.txt").

scrapper:add_worker(workerid).

---

### Scrapper state

scrapper:state().

---

