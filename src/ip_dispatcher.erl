-module(ip_dispatcher).
-behaviour(gen_server).

%% API.
-export([start_link/0]).

%% gen_server.
-export([init/1,stop/0]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-export([load_ips/1,load_cidrs/1]).
-export([get_ip/0]).
-export([state/0]).

-record(state, {
                processing = none :: atom(),
                ips = [] :: list(),
                cidrs = [] :: list(),
                current_ip = none :: tuple(),
                current_cidr = none :: tuple()
               }
       ).

%% API.

-spec start_link() -> {ok, pid()}.
start_link() ->
	gen_server:start_link({local,?MODULE},?MODULE, [], []).

%% gen_server.

init([]) ->
  %--- DISABLE TRAPPING WHEN NOT IN SUPERVISION ----
  process_flag(trap_exit, true),
	{ok, #state{}}.

%---- USED WHEN NOT IN SUPERVISION --------
stop() ->
  gen_server:cast(?MODULE,stop).

%----- TELL THE STATE ---
state() ->
  gen_server:call(?MODULE,state).

%----- LOAD CIDR FROM FILE ---
load_cidrs(Path) ->
  gen_server:cast(?MODULE,{load_cidrs,Path}).
%----- LOAD IPS FROM FILE ---
load_ips(Path) ->
  gen_server:cast(?MODULE,{load_ips,Path}).

%------ GET NEXT IP TO PROCESS -----
get_ip() ->
  gen_server:call(?MODULE,get_ip).

%----------- HANDLE --------
handle_call(get_ip,_From,State) ->
  Processing = State#state.processing,
  {NextIp,NewState} =
    case Processing of
      none ->
        {none,State};
      ips ->
        {CurrentIp,Rest} =
          case State#state.current_ip of
            no_more ->
              {no_more,[]};
            _ ->
              case State#state.ips of
                [] ->
                  {no_more,[]};
                [Ip|RestIp] ->
                  {Ip,RestIp}
              end
          end,
        {CurrentIp,State#state{current_ip = CurrentIp,ips = Rest}};
      cidrs ->
        CurrentCidr = State#state.current_cidr,
        CurrentIp = State#state.current_ip,
        Cidrs = State#state.cidrs,
        {Ip,Cidr,StateCidrs} = get_ip_from_cidr(CurrentIp,CurrentCidr,Cidrs),
        {Ip,State#state{current_ip = Ip,current_cidr = Cidr,cidrs = StateCidrs}}
    end, 
  {reply,NextIp,NewState};   
handle_call(state,_From,State) ->
  Processing = State#state.processing,
  Response =
    case Processing of
      none ->
        "Load either cidrs[ip_dispatch:load_cidrs(FilePath)] or ips[ip_dispatcher:load_ips(FilePath)]";
      ips ->
        CurrentIp = State#state.current_ip,
        case CurrentIp of
          none ->
            "Ready to Process Ips";
          no_more ->
            "Processed All Ips";
          _ ->
            Ips = State#state.ips,
            "Processing Ips, Remaining:" ++ integer_to_list(length(Ips))
        end;
      cidrs ->
        CurrentCidr = State#state.current_cidr,
        case CurrentCidr of
          none ->
            "Ready to process cidrs";
          no_more ->
            "Processed All Cidrs";
          _ ->
            Cidrs = State#state.cidrs,
            "Processing Cidrs, Remaining:" ++ integer_to_list(length(Cidrs))
        end
    end,
  {reply,Response,State};
handle_call(_Request, _From, State) ->
	{reply, ignored, State}.

handle_cast({load_cidrs,Path},State) ->
  {ReadStatus,Data} = file:read_file(Path),
  NewState =
    case ReadStatus of
      ok ->
        Rows = string:tokens(binary_to_list(Data),"\r\n\t"),
        CIDRs = [get_ip_range(Row) || Row <- Rows],
        io:format("Loaded Cidrs.~n",[]),
        State#state{current_ip = none,current_cidr = none,cidrs = CIDRs,processing = cidrs};
      error ->
        io:format("Error Reading File: ~p~n",[Data]),
        State
    end,
  {noreply,NewState};
handle_cast({load_ips,Path},State) ->
  {ReadStatus,Data} = file:read_file(Path),
  NewState =
    case ReadStatus of
      ok ->
        Rows = string:tokens(binary_to_list(Data),"\r\n\t"),
        IPs = [get_ip_from_str(Row) || Row <- Rows],
        io:format("Loaded Ips.~n",[]),
        State#state{current_ip = none,ips = IPs,processing = ips};
      error ->
        io:format("Error Reading File: ~p~n",[Data]),
        State
    end,
  {noreply,NewState};
handle_cast(stop,State) ->
  {stop,normal,State};
handle_cast(_Msg, State) ->
	{noreply, State}.

handle_info(_Info, State) ->
	{noreply, State}.

terminate(_Reason, _State) ->
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%------------ FUNCTIONS ------------
get_ip_range(CidrStr) ->
  [IP,MaskStr] = string:tokens(CidrStr,"/"),
  Mask = list_to_integer(MaskStr),
  {ok,IpTuple} = inet:parse_address(IP),
  <<CIDR:4/binary>> = list_to_binary(tuple_to_list(IpTuple)),
  HostBits = 32 - Mask,
  {list_to_tuple(binary_to_list(<<CIDR:Mask/bits, 0:HostBits/integer>>)),
   list_to_tuple(binary_to_list(<<CIDR:Mask/bits, 16#FFFFFFFF:HostBits/integer>>))
  }.

get_ip_from_str(Str) ->
  {ok,IP} = inet:parse_address(Str),
  IP.

get_ip_from_cidr(no_more,no_more,[]) ->
  {no_more,no_more,[]};
get_ip_from_cidr(none,none,[]) ->
  {no_more,no_more,[]};
get_ip_from_cidr(Ip,{_Start,Ip},[]) ->
  {no_more,no_more,[]};
get_ip_from_cidr(none,none,[Cidr|RestCidr]) ->
  {Start,_End} = Cidr,
  {Start,Cidr,RestCidr};
get_ip_from_cidr(Ip,{_Start,Ip},[Cidr|RestCidr]) ->
  {Start,_End} = Cidr,
  {Start,Cidr,RestCidr};
get_ip_from_cidr(Ip,Cidr,Cidrs) ->
  NextIp = get_next_ip(Ip),
  {NextIp,Cidr,Cidrs}.
  
get_next_ip({A,B,C,D}) ->
  IP = <<A:8,B:8,C:8,D:8>>,
  <<Bin:32>> = IP,
  <<W:8,X:8,Y:8,Z:8>> = <<(Bin+1):32>>,
  {W,X,Y,Z}.
