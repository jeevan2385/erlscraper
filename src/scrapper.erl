-module(scrapper).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).
-export([load_cidrs/1,load_ips/1]).
-export([state/0]).
-export([add_worker/1,remove_worker/1]).

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

load_cidrs(Path) ->
  ip_dispatcher:load_cidrs(Path).
load_ips(Path) ->
  ip_dispatcher:load_ips(Path).

state() ->
  State = ip_dispatcher:state(),
  io:format("~p~n",[State]),
  Current = length([1 || {_Id,_PID,_Type,[worker]} <- supervisor:which_children(?MODULE)]),
  io:format("Running ~p Workers.~n",[Current]).

add_worker(Number) ->
  Current = length([1 || {_Id,_PID,_Type,[worker]} <- supervisor:which_children(?MODULE)]),
  Start =
    case Number > Current of
      true ->
        Number - Current;
      false ->
        0
    end,
  [supervisor:start_child(?MODULE,#{id => random_string(6),
                                   start => {worker,start_link,[]},
                                   shutdown => brutal_kill,
                                   restart => temporary}
                        ) || _X <- lists:seq(1,Start)].
remove_worker(Number) ->
  Current = length([1 || {_Id,_PID,_Type,[worker]} <- supervisor:which_children(?MODULE)]),
  End =
    case Current >= Number of
      true ->
        Number;
      false ->
        0
    end,
  Pids = [Pid || {_Id,Pid,_Type,[worker]} <- supervisor:which_children(?MODULE)],
  {Wpids,_Rest} = lists:split(End,Pids),
  [Wpid ! shutdown || Wpid <- Wpids].

init([]) ->
	Procs = [
            #{id => ip_dispatcher,
             start => {ip_dispatcher,start_link,[]}
            }
          ],
	{ok, {{one_for_one, 1, 5}, Procs}}.

random_string(Len) ->
  Chrs = list_to_tuple("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"),
  ChrsSize = size(Chrs),
  F = fun(_, R) -> [element(rand:uniform(ChrsSize), Chrs) | R] end,
  lists:foldl(F, "", lists:seq(1, Len)).
