-module(worker).
-behaviour(gen_server).

%% API.
-export([start_link/0]).

%% gen_server.
-export([init/1]).
-export([handle_call/3]).
-export([handle_cast/2]).
-export([handle_info/2]).
-export([terminate/2]).
-export([code_change/3]).

-record(state, {
}).

%% API.

-spec start_link() -> {ok, pid()}.
start_link() ->
	gen_server:start_link(?MODULE, [], []).

%% gen_server.

init([]) ->
  self() ! fetch_email,
	{ok, #state{}}.

handle_call(_Request, _From, State) ->
	{reply, ignored, State}.

handle_cast(_Msg, State) ->
	{noreply, State}.

handle_info(shutdown,State) ->
  {stop,normal,State};
handle_info(fetch_email, State) ->
  Ip = ip_dispatcher:get_ip(),
  case Ip of
    no_more ->
      self() ! shutdown;
    none ->
      self() ! shutdown;
    _ ->
      {Status,Data} = fetch_email(Ip),
      case Status of
        ok ->
          {_Ip,Emails} = Data,
          io:format("~p~n",[Emails]);
        error ->
          ok
      end,
      self() ! fetch_email
  end,
	{noreply, State};
handle_info(_Info, State) ->
	{noreply, State}.

terminate(_Reason, _State) ->
	ok.

code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%------------- FUNCTIONS -------------
fetch_email(IP) ->
  URL = "http://" ++ inet:ntoa(IP),
  try httpc:request(get, {URL, []}, [{timeout,2000},{ssl, [{verify, verify_none}]}], []) of
    {ok, {{_, _, _}, _, Body}} ->
      case re:run(Body,"(mailto:.*)[\"|']>",[global,{capture,first,list}]) of
        {match,Matches} ->
          Emails = get_emails(Matches,[]),
          {ok,{IP,Emails}};
        nomatch ->
          {ok,{IP,[]}}
      end;
    {error,{failed_connect,_Rest}} ->
      {error,failed_connect};
    Unknown ->
      {error,Unknown}
  catch
    Exception ->
      {error,Exception}
  end.

get_emails([[Match]|Matches],Emails) ->
  MoreEmails = 
    case re:run(Match,"[\\w\\.-]+@[\\w\\.-]+\\.\\w+",[global,{capture,first,list}]) of
      {match,EmailLists} ->
        lists:usort([EmailStr || [EmailStr] <- EmailLists]);
      nomatch ->
        []
    end,
  get_emails(Matches,Emails ++ MoreEmails);
get_emails([],Emails) ->
  Emails.
